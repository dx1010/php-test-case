### Test case for php developer

 Please fork this project.

1. Create free account on [mlab.com](https://mlab.com). Create database in any location. It's mongodb cloud database and before using you must setup [php mongo driver](http://php.net/manual/en/mongodb.installation.php). Please have attention for this.

2. Please create entity Users with next fields: `id` (mongo id), `login` (string, max 32 symbols), `password` (string, max 32 symbols), `lastChange` (datetime stamp).

3. Please create entity Data with next fields: `id` (mongo id), `userId` (User entity's id), `key` (string, max 32 symbols), `data` (string, max 2048 symbols), `lastChange` (datetime stamp)

4. Please create REST API interface what have next routes:
---
    `users/` - POST (creating new user), GET (get user's information), DELETE (delete user), PUT (update user's information)

    Example user's information:
    `{ 
        "user": [ 
            { 
                "id": "someMongoId", 
                "login": "someLogin", 
                "password": "somePassword",
                "lastChange": "some stamp" 
            } 
        ] 
    }`

    `id` field must be used for all request exclude POST
    `password` field must be used only for PUT and POST requests
    `lastChange` field must be only readable

    All requests for this route must be signed by header `Authorization` what must contain `specialAdminToken`
---
---
    `data/` - POST (creating new data), GET (get user's data), DELETE (delete all data), PUT (update user's data)

    Example user's data:
    `{ 
        "data": [ 
            { 
                "id": "someDataId",
                "key": "someKeyData", 
                "data": "someData", 
                "lastChange": "some stamp" 
            } 
        ] 
    }`

    `id` field must be used for all request exclude POST
    `lastChange` field must be only readable
    
    All requests for this route must be signed by header `Authorization` what must contain `{userLogin}:{userPassword}`

For DELETE requests you can set only `id` field.
   
When you complete project, please sent link to [@borys_ermokhin (telegram)](https://t.me/borys_yermokhin). More commits - more chances for hire.

Please don't forget for tests.

Good luck!