<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Controller\UsersDBController;

class UsersController {
	private $response = array();

	public function index (Request $request){

		$DB = new UsersDBController();

		if($request->isMethod('POST') && $this->hasAuth($request) && $request->request->has('login') && $request->request->has('password')){

			$login = $request->request->get('login');
			$password = $request->request->get('password');

			$query = $DB->setUserData(array(
				'login' => $login,
				'password' => $password,
				'lastChange' => $this->getTimeStamp()
			));

			$this->response['id'] = $query['id'];
		}


		if($request->isMethod('GET') && $this->hasAuth($request) && $request->query->has('id')){
			$userId = $request->query->get('id');

			$user = $DB->getUserById($userId);

			$this->response['user'] = $user;
		}


		if($request->isMethod('DELETE') && $this->hasAuth($request) && isset($this->CRequest()['id'])){
			
			$userId = $this->CRequest()['id'];

			$query = $DB->deleteUserById($userId);

			$this->response['deleted'] = $query['deleted'];
		}


		if($request->isMethod('PUT') && $this->hasAuth($request) && isset($this->CRequest()['id'])){

			$userId = $this->CRequest()['id'];
			$userData = array();

			if(isset($this->CRequest()['login']))
				$userData['login'] = $this->CRequest()['login'];

			if(isset($this->CRequest()['password']))
				$userData['password'] = $this->CRequest()['password'];

			$userData['lastChange'] = $this->getTimeStamp();

			$query = $DB->setUserDataById($userId, $userData);

			$this->response['updated'] = $query['set'];
		}

		return new JsonResponse($this->response);
	}

	private function hasAuth($request){
		if(!$request->headers->has('Authorization')){
			$this->response['error'] = 'Wrong Request';
			return 0;
		}

		if($request->headers->get('Content-Type') != 'application/x-www-form-urlencoded'){
			$this->response['error'] = 'Content should be urlencoded';
			return 0;
		}

		if($request->headers->get('Authorization') != 'specialAdminToken'){
			$this->response['error'] = 'Wrong Token';
			return 0;
		}
		
		return 1;
	}

	private function CRequest(){
		parse_str(file_get_contents('php://input'), $data);
		return $data;
	}

	private function getTimeStamp(){
		return date("y-m-d H:i", time());
	}
}