<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Controller\DataDBController;

class DataController {
	private $response = array();

	public function index (Request $request){
		$DB = new DataDBController();

		if($request->isMethod('POST') && $this->hasAuth($request, $DB) && $request->request->has('key') && $request->request->has('data')){
			
			$key = $request->request->get('key');
			$data = $request->request->get('data');

			$userId = $this->getUserId($request, $DB);

			$query = $DB->setData(array(
				'key' => $key,
				'data' => $data,
				'userId' => $userId,
				'lastChange' => $this->getTimeStamp()
			));

			$this->response['id'] = $query['id'];
		}


		if($request->isMethod('GET') && $this->hasAuth($request, $DB) && $request->query->has('id')){

			$dataId = $request->query->get('id');
				
			$data = $DB->getDataById($dataId);

			$this->response['data'] = $data;
		}


		if($request->isMethod('DELETE') && $this->hasAuth($request, $DB) && isset($this->CRequest()['id'])){

			$dataId = $this->CRequest()['id'];

			$query = $DB->deleteDataById($dataId);

			$this->response['deleted'] = $query['deleted'];
		}


		if($request->isMethod('PUT') && $this->hasAuth($request, $DB) && isset($this->CRequest()['id'])){
			
			$dataId = $this->CRequest()['id'];


$this->response['PUT'] = $this->CRequest();


			$data = array();

			if(isset($this->CRequest()['key']))
				$data['key'] = $this->CRequest()['key'];

			if(isset($this->CRequest()['data']))
				$data['data'] = $this->CRequest()['data'];

			if(isset($this->CRequest()['userId']))
				$data['userId'] = $this->CRequest()['userId'];

			$data['lastChange'] = $this->getTimeStamp();

			$query = $DB->setDataById($dataId, $data);

			$this->response['updated'] = $query['updated'];
		}

		return new JsonResponse($this->response);
	}

	private function hasAuth($request, $db){
		if(!$request->headers->has('Authorization')){
			$this->response['error'] = 'Wrong Request';
			return 0;
		}

		if($request->headers->get('Content-Type') != 'application/x-www-form-urlencoded'){
			$this->response['error'] = 'Content should be urlencoded';
			return 0;
		}
		
		$authResponse = $this->getAuthHeader($request);		
		$userData = $db->findUser(array(
			'login' => $authResponse['login'],
			'password' => $authResponse['password']
		));
		
		if(empty($userData)){
			$this->response['error'] = 'Access Denied';
			return 0;
		}

		return 1;
	}

	private function CRequest(){
		parse_str(file_get_contents('php://input'), $data);
		return $data;
	}

	private function getTimeStamp(){
		return date("y-m-d H:i", time());
	}

	private function getAuthHeader($request){
		$header = explode(':', (string)$request->headers->get('Authorization'));

		$ret['login'] = isset($header[0]) ? $header[0] : '';
		$ret['password'] = isset($header[1]) ? $header[1] : '';

		return $ret;
	}

	private function getUserId($request, $db){
		$authResponse = $this->getAuthHeader($request);

		$userId = $db->findUser(array(
			"login" => $authResponse['login'], 
			"password" => $authResponse['password']
		));

		return $userId['id'];
	}
}

