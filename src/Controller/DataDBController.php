<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use MongoDB;

class DataDBController {
	private $client;

	public function __construct(){
		$this->client = (new MongoDB\Client("mongodb://dbuser:RwW8b7Bb@ds135413.mlab.com:35413/php_test_case"))->php_test_case;
	}

	public function setData($data){
		$query = $this->client->Data->insertOne([
				'key' => htmlspecialchars($data['key'], ENT_QUOTES, "UTF-8"),
				'data' => htmlspecialchars($data['data'], ENT_QUOTES, "UTF-8"),
				'userId' => htmlspecialchars($data['userId'], ENT_QUOTES, "UTF-8"),
				'lastChange' => htmlspecialchars($data['lastChange'], ENT_QUOTES, "UTF-8")
			]
		);

		return array('id' => (string)$query->getInsertedId());
	}

	public function getDataById($id){
		$data = $this->client->Data->find([
			'_id' => new MongoDB\BSON\ObjectId(preg_replace('/[^0-9A-Fa-f]/', '0', str_pad(substr((string)$id, 0, 24), 24))
			),
		])->toArray();

		return empty($data) ? array() : array(
			'id' => (string)$data[0]->_id,
			'key' => $data[0]->key,
			'data' => $data[0]->data,
			'userId' => $data[0]->userId,
			'lastChange' => $data[0]->lastChange
		);
	}

	public function setDataById($id, $data){
		$set = array();

		if(isset($data['key']))
			$set['key'] = htmlspecialchars($data['key'], ENT_QUOTES, "UTF-8");

		if(isset($data['data']))
			$set['data'] = htmlspecialchars($data['data'], ENT_QUOTES, "UTF-8");

		if(isset($data['userId']))
			$set['userId'] = htmlspecialchars($data['userId'], ENT_QUOTES, "UTF-8");

		if(isset($data['lastChange']))
			$set['lastChange'] = htmlspecialchars($data['lastChange'], ENT_QUOTES, "UTF-8");

		$query = $this->client->Data->updateOne(
			[ '_id' => new MongoDB\BSON\ObjectId(preg_replace('/[^0-9A-Fa-f]/', '0', str_pad(substr((string)$id, 0, 24), 24))) ],
			[ '$set' => $set ]
		);

		return array('updated' => $query->getModifiedCount());
	}

	public function deleteDataById($id){
		$query = $this->client->Data->deleteOne([
			'_id' => new MongoDB\BSON\ObjectId(preg_replace('/[^0-9A-Fa-f]/', '0', str_pad(substr((string)$id, 0, 24), 24)))
		]);

		return array('deleted' => $query->getDeletedCount());
	}


	public function findUser($data){
		$query = $this->client->Users->find(array(
			"login" => (string)$data['login'], 
			"password" => (string)$data['password']
		))->toArray();

		return empty($query) ? array() : array(
			'id' => (string)$query[0]->_id,
			'login' => $query[0]->login,
			'password' => $query[0]->password,
			'lastChange' => $query[0]->lastChange
		);
	}



}