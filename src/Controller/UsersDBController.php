<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use MongoDB;

class UsersDBController{
    private $client;

    public function __construct(){
    	$this->client = (new MongoDB\Client("mongodb://dbuser:RwW8b7Bb@ds135413.mlab.com:35413/php_test_case"))->php_test_case;
    }

    public function getUserById($id){
		$userData = $this->client->Users->find([
			'_id' => new MongoDB\BSON\ObjectId(preg_replace('/[^0-9A-Fa-f]/', '0', str_pad(substr((string)$id, 0, 24), 24))),
		])->toArray();

		return empty($userData) ? array() : array(
			'id' => (string)$userData[0]->_id,
			'login' => $userData[0]->login,
			'password' => $userData[0]->password,
			'lastChange' => $userData[0]->lastChange
		);
    }

    public function setUserData($data){
		$query = $this->client->Users->insertOne([
				'login' => htmlspecialchars($data['login'], ENT_QUOTES, "UTF-8"),
				'password' => htmlspecialchars($data['password'], ENT_QUOTES, "UTF-8"),
				'lastChange' => htmlspecialchars($data['lastChange'], ENT_QUOTES, "UTF-8")
			]
		);

		return array('id' => (string)$query->getInsertedId());
    }

    public function deleteUserById($id){	
		$query = $this->client->Users->deleteOne([
			'_id' => new MongoDB\BSON\ObjectId(preg_replace('/[^0-9A-Fa-f]/', '0', str_pad(substr((string)$id, 0, 24), 24)))
		]);

		return array('deleted' => $query->getDeletedCount());
    }

    public function setUserDataById($id, $data){
		$set = array();

		if(isset($data['login']))
			$set['login'] = htmlspecialchars($data['login'], ENT_QUOTES, "UTF-8");

		if(isset($data['password']))
			$set['password'] = htmlspecialchars($data['password'], ENT_QUOTES, "UTF-8");

		if(isset($data['lastChange']))
			$set['lastChange'] = htmlspecialchars($data['lastChange'], ENT_QUOTES, "UTF-8");

		$query = $this->client->Users->updateOne(
			[ '_id' => new MongoDB\BSON\ObjectId(preg_replace('/[^0-9A-Fa-f]/', '0', str_pad(substr((string)$id, 0, 24), 24))) ],
			[ '$set' => $set ]
		);

		return array('set' => $query->getModifiedCount());
    }

}
